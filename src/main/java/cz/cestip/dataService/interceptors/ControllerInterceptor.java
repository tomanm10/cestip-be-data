package cz.cestip.dataService.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;


/**
 * Interceptor that logs outgoing Http responses
 *
 */

@Component
public class ControllerInterceptor implements HandlerInterceptor {

    private static final Logger logger = Logger.getLogger(ControllerInterceptor.class.getName());

    /***
     *
     *  Logs method that handles incoming request, response status and IP:PORT of client
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     */

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        logger.info("Send response to method " + ((HandlerMethod) handler).getMethod().getName() +
                " with status " + response.getStatus() + " to IP " + request.getRemoteAddr() + ":" + request.getRemotePort());
    }


}
