package cz.cestip.dataService.interceptors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Simple class that adds interceptors
 */


@Component
public class ServiceInterceptorAppConfig implements WebMvcConfigurer {

    @Autowired
    ControllerInterceptor controllerInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(controllerInterceptor);
    }
}
