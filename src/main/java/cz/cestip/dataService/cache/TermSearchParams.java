package cz.cestip.dataService.cache;

import cz.cestip.dataService.model.AccommodationType;
import cz.cestip.dataService.model.Boarding;
import cz.cestip.dataService.model.Destination;
import cz.cestip.dataService.rest.util.CalendarUtil;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

public class TermSearchParams implements Serializable {
    private Calendar from;
    private Calendar to;
    private Destination destination;
    private AccommodationType accommodationType;
    private Boarding boarding;
    private Integer adults;
    private Integer kids;
    private Integer minPrice;
    private Integer maxPrice;
    private Calendar downloadDateFrom;
    private Calendar downloadDateTo;

    private String dataName;

    public TermSearchParams(
            String methodName,
            Calendar from,
            Calendar to,
            Destination destination,
            AccommodationType accommodationType,
            Boarding boarding,
            Integer adults,
            Integer kids,
            Integer minPrice,
            Integer maxPrice,
            Calendar downloadDateFrom,
            Calendar downloadDateTo
    ) {
        this.dataName = methodName;
        this.from = from;
        this.to = to;
        this.destination = destination;
        this.accommodationType = accommodationType;
        this.boarding = boarding;
        this.adults = adults;
        this.kids = kids;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.downloadDateFrom = downloadDateFrom;
        this.downloadDateTo = downloadDateTo;
    }

    public TermSearchParams changeMethodName(String newDataName){
        return new TermSearchParams(
                newDataName,
                from,
                to,
                destination,
                accommodationType,
                boarding,
                adults,
                kids,
                minPrice,
                maxPrice,
                downloadDateFrom,
                downloadDateTo
        );
    }

    private int dateHash(Calendar toBeHashed){
        if(toBeHashed==null){
            return 1;
        }
        return Objects.hash(toBeHashed.get(Calendar.DAY_OF_MONTH),toBeHashed.get(Calendar.MONTH),toBeHashed.get(Calendar.YEAR));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TermSearchParams cacheKey = (TermSearchParams) o;

        if (from != null ? !CalendarUtil.sameDay(from,cacheKey.from) : cacheKey.from != null) return false;
        if (to != null ? !CalendarUtil.sameDay(to,cacheKey.to) : cacheKey.to != null) return false;
        if (!Objects.equals(destination, cacheKey.destination)) return false;
        if (accommodationType != cacheKey.accommodationType) return false;
        if (boarding != cacheKey.boarding) return false;
        if (!Objects.equals(adults, cacheKey.adults)) return false;
        if (!Objects.equals(kids, cacheKey.kids)) return false;
        if (!Objects.equals(minPrice, cacheKey.minPrice)) return false;
        if (!Objects.equals(maxPrice, cacheKey.maxPrice)) return false;
        if (!Objects.equals(dataName, cacheKey.dataName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateHash(from), dateHash(to), destination, accommodationType, boarding, adults, kids, minPrice, maxPrice, dataName, dateHash(downloadDateFrom), dateHash(downloadDateTo));
    }


    @Override
    public String toString() {
        return "TermDaoCacheKey{" +
                "from=" + from +
                ", to=" + to +
                ", destination=" + destination +
                ", accommodationType=" + accommodationType +
                ", boarding=" + boarding +
                ", adults=" + adults +
                ", kids=" + kids +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", dataName='" + dataName + '\'' +
                '}';
    }


    public Calendar getFrom() {
        return from;
    }

    public Calendar getTo() {
        return to;
    }

    public Destination getDestination() {
        return destination;
    }

    public AccommodationType getAccommodationType() {
        return accommodationType;
    }

    public Boarding getBoarding() {
        return boarding;
    }

    public Integer getAdults() {
        return adults;
    }

    public Integer getKids() {
        return kids;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public Calendar getDownloadDateFrom() {
        return downloadDateFrom;
    }

    public Calendar getDownloadDateTo() {
        return downloadDateTo;
    }

    public String getDataName() {
        return dataName;
    }
}
