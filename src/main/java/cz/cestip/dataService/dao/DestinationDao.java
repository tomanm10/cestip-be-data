package cz.cestip.dataService.dao;

import cz.cestip.dataService.model.Destination;
import org.springframework.stereotype.Repository;

@Repository
public class DestinationDao extends BaseDao<Destination> {
    public DestinationDao() { super(Destination.class); }

    public Destination findByName(String name){
        return em.createNamedQuery("Destination.findByName", Destination.class)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }
}