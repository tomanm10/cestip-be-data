package cz.cestip.dataService.dao;

import cz.cestip.dataService.model.Trip;
import org.springframework.stereotype.Repository;

@Repository
public class TripDao extends BaseDao<Trip> {
    public TripDao() {
        super(Trip.class);
    }
}