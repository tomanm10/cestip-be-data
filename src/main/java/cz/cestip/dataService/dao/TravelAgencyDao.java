package cz.cestip.dataService.dao;

import cz.cestip.dataService.model.TravelAgency;
import org.springframework.stereotype.Repository;

@Repository
public class TravelAgencyDao extends BaseDao<TravelAgency> {

    public TravelAgencyDao() {
        super(TravelAgency.class);
    }

    public TravelAgency findByName(String name) {
        return em.createNamedQuery("TravelAgency.findByName", TravelAgency.class)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

}