package cz.cestip.dataService.dao;

import cz.cestip.dataService.model.Accommodation;
import org.springframework.stereotype.Repository;

@Repository
public class AccommodationDao extends BaseDao<Accommodation> {
    public AccommodationDao() {
        super(Accommodation.class);
    }

    public Accommodation findByName(String name){
        return em.createNamedQuery("Accommodation.findByName", Accommodation.class)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }
}
