package cz.cestip.dataService.dao;

import cz.cestip.dataService.cache.TermSearchParams;
import cz.cestip.dataService.model.*;
import cz.cestip.dataService.rest.util.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Repository
@CacheConfig(cacheNames = "data_cache")
public class TermDao extends BaseDao<Term> {

    private static final Logger logger = Logger.getLogger(TermDao.class.getName());

    private static final String FIND_ALL_DATA_NAME = "findAllMatching";

    @Autowired
    private CacheManager cacheManager;

    public TermDao() {
        super(Term.class);
    }

    @Override
    @CacheEvict(allEntries = true)
    public void persist(Term entity) {
        super.persist(entity);
    }

    @Cacheable
    public List<Term> findAllMatching(TermSearchParams toMatch) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Term> q = cb.createQuery(Term.class);

        Root<Term> term = q.from(Term.class);
        Join<Term, Trip> trip = term.join(Term_.trip);

        List<Predicate> predicates = new ArrayList<>();

        if (toMatch.getDownloadDateFrom() != null) {
            predicates.add(cb.greaterThanOrEqualTo(term.get(Term_.downloadDate), toMatch.getDownloadDateFrom()));
        }

        if (toMatch.getDownloadDateTo() != null) {
            predicates.add(cb.lessThanOrEqualTo(term.get(Term_.downloadDate), toMatch.getDownloadDateTo()));
        }

        if (toMatch.getFrom() != null) {
            predicates.add(cb.greaterThanOrEqualTo(term.get(Term_.from), toMatch.getFrom()));
        }
        if (toMatch.getTo() != null) {
            predicates.add(cb.lessThanOrEqualTo(term.get(Term_.to), toMatch.getTo()));
        }

        if (toMatch.getDestination() != null) {
            predicates.add(cb.equal(trip.get(Trip_.destination), toMatch.getDestination()));
        }

        if (toMatch.getAccommodationType() != null) {
            predicates.add(cb.equal(term.get(Term_.accommodationType), toMatch.getAccommodationType()));
        }
        if (toMatch.getBoarding() != null) {
            predicates.add(cb.equal(term.get(Term_.boarding), toMatch.getBoarding()));
        }

        if (toMatch.getAdults() != null) {
            predicates.add(cb.equal(term.get(Term_.adults), toMatch.getAdults()));
        }
        if (toMatch.getKids() != null) {
            predicates.add(cb.equal(term.get(Term_.kids), toMatch.getKids()));
        }

        if (toMatch.getMinPrice() != null) {
            predicates.add(cb.greaterThanOrEqualTo(term.get(Term_.price), toMatch.getMinPrice()));
        }
        if (toMatch.getMaxPrice() != null) {
            predicates.add(cb.lessThanOrEqualTo(term.get(Term_.price), toMatch.getMaxPrice()));
        }

        q.select(term).where(predicates.toArray(new Predicate[]{}));
        TypedQuery<Term> query = em.createQuery(q);

        return query.getResultList();
    }


    @Cacheable(key = "#toMatch")
    public List<EvolutionData> getAvgPrizeEvolution(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));

        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());
        List<EvolutionData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var data = new EvolutionData();
            data.setTravelAgency(travelAgency.getName());

            List<EvolutionPrice> prices = new ArrayList<>();
            data.setPrices(prices);

            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(toMatch.getDownloadDateFrom().getTimeInMillis());
            for (; date.before(toMatch.getDownloadDateTo()) || CalendarUtil.sameDay(date, toMatch.getDownloadDateTo()); date.add(Calendar.DAY_OF_MONTH, 1)) {
                var price = new EvolutionPrice();
                Calendar d = Calendar.getInstance();
                d.setTimeInMillis(date.getTimeInMillis());
                price.setDate(d);

                var p = terms.stream()
                        .filter(t -> CalendarUtil.sameDay(t.getDownloadDate(), date))
                        .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                        .mapToInt(Term::getPrice)
                        .average();

                if (p.isPresent()) {
                    price.setPrice((int) p.getAsDouble());
                } else {
                    price.setPrice(null);
                }

                prices.add(price);
            }

            out.add(data);
        }
        return out;
    }

    @Cacheable(key = "#toMatch")
    public List<EvolutionData> getMaxPriceEvolution(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));

        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());
        List<EvolutionData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var data = new EvolutionData();
            data.setTravelAgency(travelAgency.getName());

            List<EvolutionPrice> prices = new ArrayList<>();
            data.setPrices(prices);

            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(toMatch.getDownloadDateFrom().getTimeInMillis());
            for (; date.before(toMatch.getDownloadDateTo()) || CalendarUtil.sameDay(date, toMatch.getDownloadDateTo()); date.add(Calendar.DAY_OF_MONTH, 1)) {
                var price = new EvolutionPrice();
                Calendar d = Calendar.getInstance();
                d.setTimeInMillis(date.getTimeInMillis());
                price.setDate(d);

                var p = terms.stream()
                        .filter(t -> CalendarUtil.sameDay(t.getDownloadDate(), date))
                        .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                        .mapToInt(Term::getPrice)
                        .max();

                if (p.isPresent()) {
                    price.setPrice(p.getAsInt());
                } else {
                    price.setPrice(null);
                }

                prices.add(price);
            }

            out.add(data);
        }
        return out;
    }


    @Cacheable(key = "#toMatch")
    public List<EvolutionData> getMinPriceEvolution(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));

        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());
        List<EvolutionData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var data = new EvolutionData();
            data.setTravelAgency(travelAgency.getName());

            List<EvolutionPrice> prices = new ArrayList<>();
            data.setPrices(prices);

            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(toMatch.getDownloadDateFrom().getTimeInMillis());
            for (; date.before(toMatch.getDownloadDateTo()) || CalendarUtil.sameDay(date, toMatch.getDownloadDateTo()); date.add(Calendar.DAY_OF_MONTH, 1)) {
                var price = new EvolutionPrice();
                Calendar d = Calendar.getInstance();
                d.setTimeInMillis(date.getTimeInMillis());
                price.setDate(d);

                var p = terms.stream()
                        .filter(t -> CalendarUtil.sameDay(t.getDownloadDate(), date))
                        .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                        .mapToInt(Term::getPrice)
                        .min();

                if (p.isPresent()) {
                    price.setPrice(p.getAsInt());
                } else {
                    price.setPrice(null);
                }

                prices.add(price);
            }

            out.add(data);
        }
        return out;
    }

    @Cacheable(key = "#toMatch")
    public List<TermCount> getTermCount(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));

        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());

        List<TermCount> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var count = new TermCount();
            count.setTravelAgency(travelAgency.getName());
            count.setCount(
                    (int) terms.stream()
                            .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                            .count()
            );
            out.add(count);
        }

        return out;
    }

    @Cacheable(key = "#toMatch")
    public List<PriceData> getAvgPrice(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));


        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());

        List<PriceData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var count = new PriceData();

            count.setTravelAgency(travelAgency.getName());
            var price = terms.stream()
                    .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                    .mapToInt(Term::getPrice)
                    .average();

            if (price.isPresent()) {
                count.setPrice((int) price.getAsDouble());
                out.add(count);
            }
        }
        return out;
    }


    @Cacheable(key = "#toMatch")
    public List<PriceData> getMaxPrice(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));

        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());

        List<PriceData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var count = new PriceData();
            count.setTravelAgency(travelAgency.getName());

            var price = terms.stream()
                    .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                    .mapToInt(Term::getPrice)
                    .max();

            if (price.isPresent()) {
                count.setPrice(price.getAsInt());
                out.add(count);
            }
        }
        return out;
    }

    @Cacheable(key = "#toMatch")
    public List<PriceData> getMinPrice(TermSearchParams toMatch) {
        List<Term> terms = findAllMatching(toMatch.changeMethodName(FIND_ALL_DATA_NAME));
        List<TravelAgency> travelAgencies = terms.stream()
                .map(Term::getTrip)
                .map(Trip::getTravelAgency)
                .distinct()
                .collect(Collectors.toList());

        List<PriceData> out = new ArrayList<>(travelAgencies.size());

        for (var travelAgency : travelAgencies) {
            var count = new PriceData();
            count.setTravelAgency(travelAgency.getName());

            var price = terms.stream()
                    .filter(t -> t.getTrip().getTravelAgency().equals(travelAgency))
                    .mapToInt(Term::getPrice)
                    .min();

            if (price.isPresent()) {
                count.setPrice(price.getAsInt());
                out.add(count);
            }
        }

        return out;
    }
}