package cz.cestip.dataService.model;

public enum Boarding {
    FULL_BOARD,
    ALL_INCLUSIVE,
    HALF_BOARD,
    NONE
}
