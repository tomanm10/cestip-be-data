package cz.cestip.dataService.model;

import java.io.Serializable;

public class TermCount implements Serializable {
    private String travelAgency;
    private Integer count;

    public String getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(String travelAgency) {
        this.travelAgency = travelAgency;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
