package cz.cestip.dataService.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Trip extends AbstractEntity {
    private String description;
    private String url;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Destination destination;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private TravelAgency travelAgency;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Accommodation accommodation;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public TravelAgency getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(TravelAgency travelAgency) {
        this.travelAgency = travelAgency;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

}
