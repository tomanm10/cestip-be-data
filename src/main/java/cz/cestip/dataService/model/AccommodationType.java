package cz.cestip.dataService.model;

public enum AccommodationType {
    APARTMENT,
    HOTEL,
    BUNGALOW
}
