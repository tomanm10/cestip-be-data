package cz.cestip.dataService.model;

import java.io.Serializable;

public class PriceData implements Serializable {
    private String travelAgency;
    private Integer price;

    public String getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(String travelAgency) {
        this.travelAgency = travelAgency;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
