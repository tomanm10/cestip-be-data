package cz.cestip.dataService.model;

import javax.persistence.*;
import java.util.Calendar;

@Entity
public class Term extends AbstractEntity {
    private String uid;

    private String url;

    private Integer adults;
    private Integer kids;

    private Integer price;
    private Integer originalPrice;

    @Column(nullable = false, name = "beginning")
    @Temporal(TemporalType.DATE)
    private Calendar from;

    @Column(nullable = false, name = "ending")
    @Temporal(TemporalType.DATE)
    private Calendar to;

    @Enumerated(EnumType.STRING)
    private Boarding boarding;

    @Enumerated(EnumType.STRING)
    private AccommodationType accommodationType;

    @Temporal(TemporalType.DATE)
    private Calendar downloadDate;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Trip trip;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Calendar getFrom() {
        return from;
    }

    public void setFrom(Calendar from) {
        this.from = from;
    }

    public Integer getKids() {
        return kids;
    }

    public void setKids(Integer kids) {
        this.kids = kids;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Calendar getTo() {
        return to;
    }

    public void setTo(Calendar to) {
        this.to = to;
    }

    public Boarding getBoarding() {
        return boarding;
    }

    public void setBoarding(Boarding boarding) {
        this.boarding = boarding;
    }

    public AccommodationType getAccommodationType() {
        return accommodationType;
    }

    public void setAccommodationType(AccommodationType accommodationType) {
        this.accommodationType = accommodationType;
    }

    public Calendar getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(Calendar downloadDate) {
        this.downloadDate = downloadDate;
    }
}
