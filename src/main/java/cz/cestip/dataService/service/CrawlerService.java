package cz.cestip.dataService.service;

import cz.cestip.dataService.dao.*;
import cz.cestip.dataService.dto.CrawlerDataDto;
import cz.cestip.dataService.model.*;
import cz.cestip.dataService.rest.clients.GeoCodeApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 *  Service that is responsible for saving given CrawlerDataDto to DB as {@link Trip}
 *
 */
@Service
public class CrawlerService {
    @Autowired
    private TravelAgencyDao travelAgencyDao;

    @Autowired
    private DestinationDao destinationDao;

    @Autowired
    private AccommodationDao accommodationDao;

    @Autowired
    private TermDao termDao;

    @Autowired
    private TripDao tripDao;

    @Autowired
    private GeoCodeApiClient geoCodeApiClient;

    /**
     *  Main method of class, saves CrawlerDataDto to DB as {@link Trip} and it's {@link Term}'s. If {@link Trip } is already in DB, saves only {@link Term}'s
     *
     *  If {@link Accommodation} / {@link Destination} / {@link TravelAgency} is not in DB, this method creates it
     *
     * @param trip trip to save
     */
    @Transactional
    public void createTrip(CrawlerDataDto trip) {
        var accommodation = accommodationDao.findByName(trip.accommodation.name);
        if(accommodation==null){
            accommodation = getAccommodation(trip);

            accommodationDao.persist(accommodation);
        }

        String destName = getDestinationName(trip);

        var destination = destinationDao.findByName(destName);
        if(destination==null){
            destination = getDestination(trip);

            destinationDao.persist(destination);
        }

        var travelAgency = travelAgencyDao.findByName(trip.travelAgency);
        if(travelAgency==null){
            travelAgency = getTravelAgency(trip);

            travelAgencyDao.persist(travelAgency);
        }

        var t = tripDao.find(trip.id);
        if(t==null) {
            t = getTrip(trip);
            t.setTravelAgency(travelAgency);
            t.setDestination(destination);
            t.setAccommodation(accommodation);
            
            tripDao.persist(t);
        }

        var terms = getTerms(trip);
        for(var term : terms){
            term.setTrip(t);

            termDao.persist(term);
        }
    }

    private TravelAgency getTravelAgency(CrawlerDataDto trip){
        var t = new TravelAgency();
        t.setName(trip.travelAgency);

        return t;
    }

    private String getDestinationName(CrawlerDataDto trip){
        String destName;
        if(trip.destination.length>=2){
            destName = trip.destination[0]+", "+trip.destination[1];
        }else{
            destName = trip.destination[0];
        }
        return destName;
    }

    private Destination getDestination(CrawlerDataDto trip) {
        var d = new Destination();
        var name = getDestinationName(trip);
        d.setName(name);
        
        try {
            var geoCodes = geoCodeApiClient.getGeoCode(name, "3d1dbab4695c3a", "json");
            if(!geoCodes.isEmpty()) {
                var geoCode = geoCodes.get(0);
                d.setLat(geoCode.lat);
                d.setLng(geoCode.lon);
            }
        }catch(Exception e){
        }

        return d;
    }

    private Accommodation getAccommodation(CrawlerDataDto trip) {
        var accommodation = trip.accommodation;
        var a = new Accommodation();

        a.setClassification(accommodation.classification);
        a.setRating(accommodation.rating);
        a.setUrl(accommodation.url);
        a.setName(accommodation.name);

        try {
            var address = geoCodeApiClient.getAddress(accommodation.lat, accommodation.lon, "3d1dbab4695c3a", "json");
            a.setCountry(address.address.country);
            a.setCity(address.address.city);
            a.setStreet(address.address.road);
        }catch(Exception e){
        }

        return a;
    }

    private Trip getTrip(CrawlerDataDto trip) {
        var t = new Trip();
        t.setUrl(trip.url);
        t.setId(trip.id);

        return t;
    }

    private List<Term> getTerms(CrawlerDataDto trip) {
        var terms = trip.terms;
        List<Term> out = new ArrayList<>(terms.length);
        for(var term : terms){
            Term t = new Term();
            t.setDownloadDate(Calendar.getInstance());
            t.setAccommodationType(term.accommodationType);
            t.setBoarding(term.boarding);
            t.setOriginalPrice(term.price);
            t.setPrice(term.price);
            t.setUid(term.id);
            t.setUrl(term.url);

            t.setTo(term.to);
            t.setFrom(term.from);
            t.setKids(term.people.children);
            t.setAdults(term.people.adults);

            out.add(t);
        }

        return out;
    }
}
