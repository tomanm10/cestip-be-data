package cz.cestip.dataService.rest;

import cz.cestip.dataService.dao.TravelAgencyDao;
import cz.cestip.dataService.exception.NotFoundException;
import cz.cestip.dataService.model.Term;
import cz.cestip.dataService.model.TravelAgency;
import cz.cestip.dataService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * REST controller for {@link TravelAgency}
 */
@RestController
@RequestMapping("/travelagencies")
public class TravelAgencyController {

    @Autowired
    TravelAgencyDao travelAgencyDao;

    @GetMapping
    public List<TravelAgency> getTravelAgencies() {
        return travelAgencyDao.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TravelAgency getTravelAgency(@PathVariable int id) {
        var travelAgency =  travelAgencyDao.find(id);
        if (travelAgency == null) {
            throw NotFoundException.create("TravelAgency", id);
        }
        return travelAgency;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTravelAgency(@RequestBody TravelAgency agency) {
        travelAgencyDao.persist(agency);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", agency.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
