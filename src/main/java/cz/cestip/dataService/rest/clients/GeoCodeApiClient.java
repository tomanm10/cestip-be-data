package cz.cestip.dataService.rest.clients;

import cz.cestip.dataService.dto.GeoCodeReverseDto;
import cz.cestip.dataService.dto.GeoCodeSearchDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "locationiq", url = "https://us1.locationiq.org/v1")
public interface GeoCodeApiClient {

    @RequestMapping(method = RequestMethod.GET, value="/search.php")
    public List<GeoCodeSearchDto> getGeoCode(
            @RequestParam(required=true) String q,
            @RequestParam(required=true) String key,
            @RequestParam(required=true) String format
    );

    @RequestMapping(method = RequestMethod.GET, value="/reverse.php")
    public GeoCodeReverseDto getAddress(
            @RequestParam(required=true) Double lat,
            @RequestParam(required=true) Double lon,
            @RequestParam(required=true) String key,
            @RequestParam(required=true) String format
    );
}
