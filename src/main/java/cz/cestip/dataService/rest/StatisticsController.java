package cz.cestip.dataService.rest;

import cz.cestip.dataService.cache.TermSearchParams;
import cz.cestip.dataService.dao.DestinationDao;
import cz.cestip.dataService.dao.TermDao;
import cz.cestip.dataService.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.List;

/**
 * Controller for statistic part of our service
 *
 *
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private TermDao termDao;

    @Autowired
    private DestinationDao destinationDao;

    /**
     * Get statistics of average prices of terms from last week
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return List of {@link EvolutionData}
     */
    @GetMapping(value = "/prices/avg/evolution")
    public List<EvolutionData> getEvolutionAveragePrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar downloadTo = Calendar.getInstance();
        downloadTo.set(2020,Calendar.MAY,20);
        Calendar downloadFrom = Calendar.getInstance();
        downloadFrom.set(2020,Calendar.MAY,16);

        return termDao.getAvgPrizeEvolution(
                new TermSearchParams(
                        "avgPrizeEvol",
                        from,
                        to,
                        dest,
                        accommodationType,
                        boarding,
                        adults,
                        kids,
                        minPrice,
                        maxPrice,
                        downloadFrom,
                        downloadTo
                )
        );
    }

    /**
     * Get statistics of max prices of terms from last week
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/prices/max/evolution")
    public List<EvolutionData> getEvolutionMaxPrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar downloadTo = Calendar.getInstance();
        downloadTo.set(2020,Calendar.MAY,20);
        Calendar downloadFrom = Calendar.getInstance();
        downloadFrom.set(2020,Calendar.MAY,16);

        return termDao.getMaxPriceEvolution(
                new TermSearchParams("maxPrizeEvol", from,
                        to,
                        dest,
                        accommodationType,
                        boarding,
                        adults,
                        kids,
                        minPrice,
                        maxPrice,
                        downloadFrom,
                        downloadTo)
        );
    }

    /**
     * Get statistics of min prices of terms from last week
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/prices/min/evolution")
    public List<EvolutionData> getEvolutionMinPrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar downloadTo = Calendar.getInstance();
        downloadTo.set(2020,Calendar.MAY,20);
        Calendar downloadFrom = Calendar.getInstance();
        downloadFrom.set(2020,Calendar.MAY,16);

        return termDao.getMinPriceEvolution(
                new TermSearchParams("minPrizeEvol",
                        from,
                        to,
                        dest,
                        accommodationType,
                        boarding,
                        adults,
                        kids,
                        minPrice,
                        maxPrice,
                        downloadFrom,
                        downloadTo)
        );
    }

    /**
     * Get statistics of average prices of currently offered terms (downloaded today or yesterday)
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/prices/avg")
    public List<PriceData> getAveragePrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar now = Calendar.getInstance();
        now.set(2020,Calendar.MAY,20);
        Calendar yesterday = Calendar.getInstance();
        yesterday.set(2020,Calendar.MAY,17);


        return termDao.getAvgPrice(new TermSearchParams("avgPrize", from, to, dest, accommodationType, boarding, adults, kids, minPrice, maxPrice,  yesterday,now));
    }

    /**
     * Get statistics of max prices of currently offered terms (downloaded today or yesterday)
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/prices/max")
    public List<PriceData> getMaxPrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar now = Calendar.getInstance();
        now.set(2020,Calendar.MAY,20);
        Calendar yesterday = Calendar.getInstance();
        yesterday.set(2020,Calendar.MAY,19);

        return termDao.getMaxPrice(new TermSearchParams("maxPrize", from, to, dest, accommodationType, boarding, adults, kids, minPrice, maxPrice, yesterday,now));
    }

    /**
     * Get statistics of min prices of currently offered terms (downloaded today or yesterday)
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/prices/min")
    public List<PriceData> getMinPrices(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar now = Calendar.getInstance();
        now.set(2020,Calendar.MAY,20);
        Calendar yesterday = Calendar.getInstance();
        yesterday.set(2020,Calendar.MAY,19);

        return termDao.getMinPrice(new TermSearchParams("minPrize", from, to, dest, accommodationType, boarding, adults, kids, minPrice, maxPrice,  yesterday,now));
    }

    /**
     * Get statistics of term counts of currently offered terms (downloaded today or yesterday)
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @return
     */
    @GetMapping(value = "/term-count")
    public List<TermCount> getTermCounts(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice
    ) {
        var dest = destinationDao.findByName(destination);

        //Hardcoded to 2020-05-20 because we stopped our crawler after that
        Calendar now = Calendar.getInstance();
        now.set(2020,Calendar.MAY,20);
        Calendar yesterday = Calendar.getInstance();
        yesterday.set(2020,Calendar.MAY,19);

        return termDao.getTermCount(
                new TermSearchParams(
                        "termCount",
                        from,
                        to,
                        dest,
                        accommodationType,
                        boarding,
                        adults,
                        kids,
                        minPrice,
                        maxPrice,
                        yesterday,
                        now
                )
        );
    }

}
