package cz.cestip.dataService.rest;

import cz.cestip.dataService.dao.DestinationDao;
import cz.cestip.dataService.exception.NotFoundException;
import cz.cestip.dataService.model.Accommodation;
import cz.cestip.dataService.model.Destination;
import cz.cestip.dataService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for {@link Destination}
 */
@RestController
@RequestMapping("/destinations")
public class DestinationController {

    @Autowired
    private DestinationDao destinationDao;

    @GetMapping
    public List<Destination> getDestinations() {
        return destinationDao.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Destination getDestination(@PathVariable int id){
        var destination =  destinationDao.find(id);
        if (destination == null) {
            throw NotFoundException.create("Destination", id);
        }
        return destination;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createDestination(@RequestBody Destination destination){
        destinationDao.persist(destination);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", destination.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
