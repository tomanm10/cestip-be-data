package cz.cestip.dataService.rest;

import cz.cestip.dataService.dao.AccommodationDao;
import cz.cestip.dataService.exception.NotFoundException;
import cz.cestip.dataService.model.Accommodation;
import cz.cestip.dataService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  REST controller for {@link Accommodation}
 */
@RestController
@RequestMapping("/accommodations")
public class AccommodationController {

    @Autowired
    AccommodationDao accommodationDao;

    @GetMapping
    public List<Accommodation> getAccommodations(){
        return accommodationDao.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Accommodation getAccommodation(@PathVariable int id){
        var accommodation =  accommodationDao.find(id);
        if (accommodation == null) {
            throw NotFoundException.create("Accommodation", id);
        }
        return accommodation;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTrip(@RequestBody Accommodation accommodation){
        accommodationDao.persist(accommodation);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", accommodation.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
