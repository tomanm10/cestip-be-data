package cz.cestip.dataService.rest;

import cz.cestip.dataService.dao.TripDao;
import cz.cestip.dataService.exception.NotFoundException;
import cz.cestip.dataService.model.Term;
import cz.cestip.dataService.model.Trip;
import cz.cestip.dataService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * REST controller for {@link Trip}
 */
@RestController
@RequestMapping("/trips")
public class TripController {
    @Autowired
    TripDao tripDao;

    @GetMapping
    public List<Trip> getTrips(){
        return tripDao.findAll();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Trip getTrip(@PathVariable int id){
        var trip =  tripDao.find(id);
        if (trip == null) {
            throw NotFoundException.create("Trip", id);
        }
        return trip;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTrip(@RequestBody Trip trip){
        tripDao.persist(trip);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", trip.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
