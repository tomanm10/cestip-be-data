package cz.cestip.dataService.rest;

import cz.cestip.dataService.cache.TermSearchParams;
import cz.cestip.dataService.dao.DestinationDao;
import cz.cestip.dataService.dao.TermDao;
import cz.cestip.dataService.exception.NotFoundException;
import cz.cestip.dataService.model.*;
import cz.cestip.dataService.rest.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

/**
 * REST controller for {@link Term}
 */
@RestController
@RequestMapping("/terms")
public class TermController {

    @Autowired
    private TermDao termDao;

    @Autowired
    private DestinationDao destinationDao;

    /**
     * Basic method for getting all terms that match given parameters.
     *
     * @param from
     * @param to
     * @param destination
     * @param accommodationType
     * @param boarding
     * @param adults
     * @param kids
     * @param minPrice
     * @param maxPrice
     * @param downloadDateFrom  Latest date in which was term downloaded
     * @param downloadDateTo    Earliest date in which was term downloaded
     * @return
     */
    @GetMapping
    public List<Term> getTerms(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar from,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar to,
            @RequestParam(required = false) String destination,
            @RequestParam(required = false) AccommodationType accommodationType,
            @RequestParam(required = false) Boarding boarding,
            @RequestParam(required = false) Integer adults,
            @RequestParam(required = false) Integer kids,
            @RequestParam(required = false) Integer minPrice,
            @RequestParam(required = false) Integer maxPrice,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar downloadDateFrom,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Calendar downloadDateTo
    ) {
        Destination dest = destinationDao.findByName(destination);
        return termDao.findAllMatching(
                new TermSearchParams(
                        "findAllMatching",
                        from,
                        to,
                        dest,
                        accommodationType,
                        boarding,
                        adults,
                        kids,
                        minPrice,
                        maxPrice,
                        downloadDateFrom,
                        downloadDateTo
                )
        );
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Term getTerm(@PathVariable int id) {
        var term = termDao.find(id);
        if (term == null) {
            throw NotFoundException.create("Term", id);
        }
        return term;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTerm(@RequestBody Term term) {
        termDao.persist(term);

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", term.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
