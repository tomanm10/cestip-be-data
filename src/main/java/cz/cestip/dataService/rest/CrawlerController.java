package cz.cestip.dataService.rest;

import cz.cestip.dataService.dto.CrawlerDataDto;
import cz.cestip.dataService.service.CrawlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Crawler service communicates with data service via this controller.
 *
 *
 *
 */
@RestController
@RequestMapping("/crawler")
public class CrawlerController {

    @Autowired
    public CrawlerService crawlerService;

    @PostMapping(value = "/trip", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createTrip(@RequestBody CrawlerDataDto trip) {
        crawlerService.createTrip(trip);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
