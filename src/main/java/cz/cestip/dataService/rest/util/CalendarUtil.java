package cz.cestip.dataService.rest.util;

import java.util.Calendar;

public class CalendarUtil {
    public static boolean sameDay(Calendar cal1, Calendar cal2){
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }

}
