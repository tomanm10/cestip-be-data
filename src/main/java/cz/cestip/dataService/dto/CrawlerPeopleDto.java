package cz.cestip.dataService.dto;

public class CrawlerPeopleDto {
    public Integer adults;
    public Integer children;
    public Integer[] childrenAge;
}
