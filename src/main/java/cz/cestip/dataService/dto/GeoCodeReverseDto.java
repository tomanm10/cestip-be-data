package cz.cestip.dataService.dto;

public class GeoCodeReverseDto {
    public Address address;

    public static class Address {
        public String country;
        public String city;
        public String road;
    }
}
