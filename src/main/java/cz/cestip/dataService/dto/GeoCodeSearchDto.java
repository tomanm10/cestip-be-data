package cz.cestip.dataService.dto;

public class GeoCodeSearchDto {
    public Double lat;
    public Double lon;
}
