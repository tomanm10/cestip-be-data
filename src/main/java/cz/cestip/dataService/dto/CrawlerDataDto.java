package cz.cestip.dataService.dto;

import java.util.Arrays;

public class CrawlerDataDto {

    public int id;
    public String url;
    public String name;
    public String[] destination;
    public String travelAgency;

    public CrawlerAccommodationDto accommodation;
    public CrawlerTermDto[] terms;

    @Override
    public String toString() {
        return "CrawlerDataDto{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", destination=" + Arrays.toString(destination) +
                ", travelAgency='" + travelAgency + '\'' +
                ", accommodation=" + accommodation +
                ", terms=" + Arrays.toString(terms) +
                '}';
    }
}
