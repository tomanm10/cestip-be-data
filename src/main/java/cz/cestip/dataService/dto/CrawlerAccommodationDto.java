package cz.cestip.dataService.dto;

public class CrawlerAccommodationDto {

    public int classification;
    public String url;
    public int rating;
    public String name;
    public Double lat;
    public Double lon;

    @Override
    public String toString() {
        return "CrawlerAccommodationDto{" +
                "classification=" + classification +
                ", url='" + url + '\'' +
                ", rating=" + rating +
                '}';
    }
}
