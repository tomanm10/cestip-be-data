package cz.cestip.dataService.dto;

import cz.cestip.dataService.model.AccommodationType;
import cz.cestip.dataService.model.Boarding;

import java.util.Calendar;

public class CrawlerTermDto {

    public String id;
    public String url;
    public Calendar from;
    public Calendar to;
    public Boarding boarding;
    public int price;
    public AccommodationType accommodationType;
    public CrawlerPeopleDto people;

    @Override
    public String toString() {
        return "CrawlerTermDto{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", boarding=" + boarding +
                ", price=" + price +
                ", accommodationType=" + accommodationType +
                '}';
    }
}
