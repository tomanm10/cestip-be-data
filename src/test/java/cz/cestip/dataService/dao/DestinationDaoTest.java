package cz.cestip.dataService.dao;

import cz.cestip.dataService.DataServiceApplication;
import cz.cestip.dataService.model.Destination;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackageClasses = DataServiceApplication.class, excludeFilters = {})
public class DestinationDaoTest {

    @Autowired
    private DestinationDao dao;
    
    @Test
    public void findByNameReturnsCorrectDestinationByName() {
        Destination destination = new Destination();
        destination.setName("Liberland");
        
        dao.persist(destination);

        Destination result = dao.findByName(destination.getName());
        
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo(destination.getName());
    }
    
    @Test
    public void findByNameReturnsNullIfNoResultsForGivenName() {
        Destination result = dao.findByName("HouskaHouskaRohlikDalamanek");
        
        assertThat(result).isNull();
    }
    
    @Test(expected = PersistenceException.class)
    public void singlePersistRejectsNameDuplicities() {
        final String DUPLICATE_NAME = "Liberland";
        
        Destination destination = new Destination();
        destination.setName(DUPLICATE_NAME);
        destination.setDescription("Nejlepsi zeme na svete");
        
        Destination duplicate = new Destination();
        destination.setName(DUPLICATE_NAME);
        destination.setDescription("Moje oblibena zeme, mam ji rad");
        
        dao.persist(destination);
        dao.persist(duplicate);
    }
    
    @Test(expected = PersistenceException.class)
    public void multiplePersistRejectsNameDuplicities() {
        final String DUPLICATE_NAME = "Liberland";
        
        Destination destination = new Destination();
        destination.setName(DUPLICATE_NAME);
        destination.setDescription("Nejlepsi zeme na svete");
        
        Destination duplicate = new Destination();
        destination.setName(DUPLICATE_NAME);
        destination.setDescription("Moje oblibena zeme, mam ji rad");
        

        dao.persist(
            Arrays.asList(destination, duplicate)
        );
    }

}
