package cz.cestip.dataService.dao;

import cz.cestip.dataService.DataServiceApplication;
import cz.cestip.dataService.cache.TermSearchParams;
import cz.cestip.dataService.environment.Generator;
import cz.cestip.dataService.model.Term;
import cz.cestip.dataService.model.Trip;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan(basePackageClasses = DataServiceApplication.class, excludeFilters = {})
public class TermDaoTest {

    @Autowired
    private TermDao termDao;

    @Test
    public void findAllMatchingTest_ExactMatch() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching", term.getFrom(),
                        term.getTo(),
                        trip.getDestination(),
                        term.getAccommodationType(),
                        term.getBoarding(),
                        term.getAdults(),
                        term.getKids(),
                        term.getPrice(),
                        term.getPrice(),
                        term.getDownloadDate(),
                        term.getDownloadDate())
        );

        assertThat(results).contains(term);
    }

    @Test
    public void findAllMatchingTest_WrongKidCount() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",
                        term.getFrom(),
                        term.getTo(),
                        trip.getDestination(),
                        term.getAccommodationType(),
                        term.getBoarding(),
                        term.getAdults(),
                        term.getKids() + 2,
                        term.getPrice(),
                        term.getPrice(),
                        term.getDownloadDate(),
                        term.getDownloadDate())
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_MissingParamsContains() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null)
        );

        assertThat(results).contains(term);
    }

    @Test
    public void findAllMatchingTest_WrongDestination() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching", null,
                        null,
                        Generator.generateDestination(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_FromBeforeStart() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        Calendar from = Calendar.getInstance();
        from.setTimeInMillis(term.getFrom().getTimeInMillis() - 86400001);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",
                from,
                null,
                trip.getDestination(),
                null,
                null,
                null,
                null,
                null,
                null,
                        null,
                        null)
        );

        assertThat(results).contains(term);
    }

    @Test
    public void findAllMatchingTest_FromBeforeStart_ToBeforeEnd() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        Calendar from = Calendar.getInstance();
        from.setTimeInMillis(term.getFrom().getTimeInMillis() - 86400001);

        Calendar to = Calendar.getInstance();
        to.setTimeInMillis(term.getFrom().getTimeInMillis() - 86400001);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        from,
                to,
                trip.getDestination(),
                null,
                null,
                null,
                null,
                null,
                null,
                        null,
                        null)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_FromAfterStart() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        Calendar from = Calendar.getInstance();
        from.setTimeInMillis(term.getFrom().getTimeInMillis() + 86400001);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        from,
                term.getTo(),
                trip.getDestination(),
                null,
                null,
                null,
                null,
                null,
                null,
                        null,
                        null)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_MinPriceTooHigh() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        null,
                null,
                null,
                null,
                null,
                null,
                null,
                term.getPrice() + 100,
                null,
                        null,
                        null)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_MaxPriceTooLow() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                term.getPrice() - 100,
                        null,
                        null)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void findAllMatchingTest_PriceIsInRange() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        null,
                null,
                null,
                null,
                null,
                null,
                null,
                term.getPrice() - 100,
                term.getPrice() + 100,
                        null,
                        null)
        );

        assertThat(results).contains(term);
    }

    @Test
    public void findAllMatchingTest_DownloadedInRange() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();

        Calendar downloadDate = Calendar.getInstance();
        downloadDate.setTimeInMillis(System.currentTimeMillis());
        term.setDownloadDate(downloadDate);

        term.setTrip(trip);

        termDao.persist(term);

        Calendar downloadDateFrom = Calendar.getInstance();
        downloadDateFrom.setTimeInMillis(System.currentTimeMillis() - 86400001);
        Calendar downloadDateTo = Calendar.getInstance();
        downloadDateTo.setTimeInMillis(System.currentTimeMillis() + 3 * 86400001);


        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                        downloadDateFrom,
                        downloadDateTo)
        );

        assertThat(results).contains(term);
    }

    @Test
    public void findAllMatchingTest_DownloadedOutOfRange_TooOld() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();

        Calendar downloadDate = Calendar.getInstance();
        downloadDate.setTimeInMillis(System.currentTimeMillis());
        term.setDownloadDate(downloadDate);

        term.setTrip(trip);

        termDao.persist(term);

        Calendar downloadDateFrom = Calendar.getInstance();
        downloadDateFrom.setTimeInMillis(System.currentTimeMillis() + 86400001);
        Calendar downloadDateTo = Calendar.getInstance();
        downloadDateTo.setTimeInMillis(System.currentTimeMillis() + 3 * 86400001);

        List<Term> results = termDao.findAllMatching(
                new TermSearchParams("findAllMatching",

                        null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                        downloadDateFrom,
                        downloadDateTo)
        );

        assertThat(results).doesNotContain(term);
    }

    @Test
    public void persistTest_ValidReferenceToTrip() {
        Trip trip = Generator.generateTrip();
        Term term = Generator.generateTerm();
        term.setTrip(trip);

        termDao.persist(term);

        assertThat(termDao.find(term.getId()).getTrip()).isEqualTo(trip);
    }
}
