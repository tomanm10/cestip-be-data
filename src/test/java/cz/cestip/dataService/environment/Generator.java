package cz.cestip.dataService.environment;

import cz.cestip.dataService.model.*;

import java.util.Calendar;

public class Generator {
    public static Trip generateTrip(){
        Trip out = new Trip();
        out.setDescription("Test trip "+Math.random());
        out.setUrl("Test Trip Url "+Math.random());
        out.setDestination(generateDestination());
        out.setTravelAgency(generateTravelAgency());
        out.setAccommodation(generateAccommodation());
        return out;
    }

    public static Destination generateDestination(){
        Destination out = new Destination();
        out.setName("Test destination "+Math.random());
        out.setDescription("Test destination description "+Math.random());
        return out;
    }

    public static TravelAgency generateTravelAgency(){
        TravelAgency out = new TravelAgency();
        out.setName("Test travel agency "+Math.random());
        out.setDescription("Test destination description "+Math.random());
        out.setRating((int) Math.round(Math.random()*5));
        out.setUrl("Test agency url "+Math.random());
        return out;
    }

    public static Accommodation generateAccommodation(){
        Accommodation out = new Accommodation();

        out.setName("Test accommodation "+Math.random());
        out.setDescription("Test accommodation description "+Math.random());

        out.setCountry("Test accommodation country "+Math.random());
        out.setCity("Test accommodation city "+Math.random());
        out.setStreet("Test accommodation street "+Math.random());

        out.setRating((int) Math.round(Math.random()*5));
        out.setClassification((int) Math.round(Math.random()*5));

        out.setUrl("Test accommodation url "+Math.random());
        return out;
    }

    public static Term generateTerm(){
        Term out = new Term();
        out.setAdults((int) Math.round(Math.random()*5));
        out.setKids((int) Math.round(Math.random()*5));

        out.setPrice((int) Math.round(Math.random()*50000));
        out.setOriginalPrice((int) Math.round(Math.random()*50000));

        Calendar from = Calendar.getInstance();
        from.setTimeInMillis(Math.round(Math.random()*1586206297420L));
        out.setFrom(from);

        Calendar to = Calendar.getInstance();
        to.setTimeInMillis(604800000L+from.getTimeInMillis());
        out.setTo(to);

        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(System.currentTimeMillis());
        out.setDownloadDate(now);

        out.setBoarding(Boarding.ALL_INCLUSIVE);

        out.setAccommodationType(AccommodationType.HOTEL);

        out.setTrip(generateTrip());

        return out;
    }
}
