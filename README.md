# CesTip BE Data

## Inicializace databáze

Datová služba používá PostgreSQL databázi.

- Název DB: cestipdata
- User: cestipdata
- Heslo: cestipdata-420-69
- Port: 5432 (standardní)

Projekt obsahuje dump z produkční databáze (obsahující nacrawlovaná data od 2020-05-16 do 2020-05-20).

Dump naleznete v souboru `data/public.sql`.